package com.example.ads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Subtitle_listView_adapter extends BaseAdapter {

    private Topic topics;
    private Context context;
    // RecyclerView recyclerView;
    public Subtitle_listView_adapter( Context context,Topic topic) {
        this.topics = topic;
        this.context = context;
    }

    @Override
    public int getCount() {
        return topics.child.size();
    }

    @Override
    public Object getItem(int position) {
        return topics.child.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        view= layoutInflater.inflate(R.layout.subtitle_list_data, parent, false);
        TextView name = view.findViewById(R.id.name);

        String title = topics.child.get(position);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(position+1);
        stringBuilder.append(". ");
        stringBuilder.append(title);

        name.setText(stringBuilder.toString());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataManager.getInstance().setSelectedTopic(topics);
                Intent intent = new Intent(context, SelectedModuleActivity.class);
                context.startActivity(intent);
            }
        });

        return view;
    }
}
