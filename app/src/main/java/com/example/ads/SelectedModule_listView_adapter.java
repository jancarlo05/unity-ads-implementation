package com.example.ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.unity3d.ads.UnityAds;

import java.util.ArrayList;

public class SelectedModule_listView_adapter extends BaseAdapter {

    private ArrayList<String> topics;
    private Context context;
    private Activity activity;
    private int counter = 0;
    // RecyclerView recyclerView;
    public SelectedModule_listView_adapter(Context context, ArrayList<String>topics, Activity activity) {
        this.topics = topics;
        this.context = context;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return topics.size();
    }

    @Override
    public Object getItem(int position) {
        return topics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        view= layoutInflater.inflate(R.layout.selected_list_data, parent, false);
        TextView name = view.findViewById(R.id.name);
        Button start = view.findViewById(R.id.start);

        String title = topics.get(position);
        StringBuilder stringBuilder = new StringBuilder();

        if (title.contains("-")){
            stringBuilder.append("   ");
            start.setVisibility(View.GONE);
        }
        stringBuilder.append(title);

        name.setText(stringBuilder.toString());


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String>videoAds= new ArrayList<>();
                videoAds.add("0621202014");
                videoAds.add("rewardedVideo");
                videoAds.add("0902173");
                videoAds.add("0902175");
                videoAds.add("062120202");
                videoAds.add("062120208");
                videoAds.add("video");

                UnityAds.initialize (activity, "3341322");

                for (String ads : videoAds){
                    UnityAds.show(activity,ads);
                }
            }
        });



        return view;
    }
}
