package com.example.ads;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;
import com.unity3d.services.UnityServices;
import com.unity3d.services.ads.adunit.IAdUnitViewHandler;
import com.unity3d.services.banners.BannerErrorInfo;
import com.unity3d.services.banners.BannerView;
import com.unity3d.services.banners.IUnityBannerListener;
import com.unity3d.services.banners.UnityBannerSize;
import com.unity3d.services.banners.UnityBanners;
import com.unity3d.services.banners.api.Banner;
import com.unity3d.services.banners.view.BannerPosition;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, IUnityAdsListener {


    private LinearLayout container1,container2,container3;
    private TextView message1,message2,message3;
    private AdView mAdView;
    private CheckBox checkBox;
    private InterstitialAd mInterstitialAd;
    private String unityGameId = "3341322";
    private Boolean testMode = true;
    private String placementId = "090217";
    private String banner2 = "0902172";
    private String banner3 = "0902174";


    private View bannerView;
    private BannerView bannerview;
    LinearLayout root;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        root = findViewById(R.id.root);
        container1 = findViewById(R.id.banner1_container);
        container2 = findViewById(R.id.banner2_container);
        container3 = findViewById(R.id.banner3_container);
        message1 = findViewById(R.id.banner1_message);
        checkBox = findViewById(R.id.checkbox);


        AdsProcess();
    }

    private int getPrevYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR,-1);
        return calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
    }

    private int getFutureYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR,+1);
        return calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
    }

    void getStartEndOFWeek(int enterWeek){

        SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd, yyyy"); // PST`

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.setFirstDayOfWeek(Calendar.MONDAY);

        ArrayList<Date> Weekdays = new ArrayList<>();

        int delta = -calendar.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
        calendar.add(Calendar.DAY_OF_MONTH, delta );
        for (int i = 0; i < 7; i++) {
            Weekdays.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        System.out.println("Start : "+formatter.format(Weekdays.get(0)));
        System.out.println("End : "+formatter.format(Weekdays.get(Weekdays.size()-1)));
    }

    private void AdsProcess(){

        final Activity myActivity = this;
        // Declare a new banner listener, and set it as the active banner listener:
        // Initialize the Ads SDK:
        UnityAds.initialize (this, unityGameId);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Toast.makeText(myActivity, "VIDEO ADS ENABLED", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(myActivity, "VIDEO ADS DISABLED", Toast.LENGTH_SHORT).show();
                }
            }
        });

        final int[] banner_ads_count = {0};

        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {


            }
            @Override
            public void onFinish() {
                banner_ads_count[0] = banner_ads_count[0] +3;

                message1.setText("Banner Count : "+banner_ads_count[0]);
                getBanner1();
                getBanner2();
                getBanner3();
                start();

            }
        }.start();

    }

    private void Inter(){
        new CountDownTimer(3000,1000){
            @Override
            public void onTick(long millisUntilFinished) {

            }
            @Override
            public void onFinish() {
                if (checkBox.isChecked()){
                    UnityAds.show(MainActivity.this,"video");
                    UnityAds.show(MainActivity.this,"0902175");
                }
                start();
            }
        }.start();
    }

    private void getBanner3(){

        BannerView bannerView = new BannerView(this,banner3, UnityBannerSize.getDynamicSize(this));
        bannerView.setListener(new BannerView.IListener() {
            @Override
            public void onBannerLoaded(BannerView bannerView) {
                System.out.println("banner load");
            }

            @Override
            public void onBannerClick(BannerView bannerView) {

            }

            @Override
            public void onBannerFailedToLoad(BannerView bannerView, BannerErrorInfo bannerErrorInfo) {
                System.out.println("banner load");
                bannerView.load();
            }

            @Override
            public void onBannerLeftApplication(BannerView bannerView) {
                bannerView.load();
            }


        });
        bannerView.load();
        container3.addView(bannerView);




    }

    private void getBanner2(){

        BannerView bannerView = new BannerView(this,banner2, UnityBannerSize.getDynamicSize(this));
        bannerView.setListener(new BannerView.IListener() {
            @Override
            public void onBannerLoaded(BannerView bannerView) {
                System.out.println("banner load");
            }

            @Override
            public void onBannerClick(BannerView bannerView) {

            }

            @Override
            public void onBannerFailedToLoad(BannerView bannerView, BannerErrorInfo bannerErrorInfo) {
                System.out.println("banner load");
                bannerView.load();
            }

            @Override
            public void onBannerLeftApplication(BannerView bannerView) {
                bannerView.load();
            }
        });
        bannerView.load();
        container2.addView(bannerView);


    }

    private void getBanner1(){

        BannerView bannerView = new BannerView(this,placementId, UnityBannerSize.getDynamicSize(this));
        bannerView.setListener(new BannerView.IListener() {
            @Override
            public void onBannerLoaded(BannerView bannerView) {
                System.out.println("banner load");
            }

            @Override
            public void onBannerClick(BannerView bannerView) {

            }

            @Override
            public void onBannerFailedToLoad(BannerView bannerView, BannerErrorInfo bannerErrorInfo) {
                System.out.println("banner load");
                bannerView.load();
            }

            @Override
            public void onBannerLeftApplication(BannerView bannerView) {
                bannerView.load();
            }
        });
        bannerView.load();
        container1.addView(bannerView);


    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onUnityAdsReady(String s) {

    }

    @Override
    public void onUnityAdsStart(String s) {

    }

    @Override
    public void onUnityAdsFinish(String s, UnityAds.FinishState finishState) {

    }

    @Override
    public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String s) {

    }
}
