package com.example.ads;

import java.util.ArrayList;

public class DataManager {
    public  static  DataManager instance = new DataManager();


    private Topic selectedTopic;

    public Topic getSelectedTopic() {
        return selectedTopic;
    }

    public void setSelectedTopic(Topic selectedTopic) {
        this.selectedTopic = selectedTopic;
    }

    private ArrayList<Topic>topics = new ArrayList<>();

    public ArrayList<Topic> getTopics() {
        return topics;
    }

    public void setTopics(ArrayList<Topic> topics) {
        this.topics = topics;
    }

    public static DataManager getInstance() {
        return instance;
    }

    public static void setInstance(DataManager instance) {
        DataManager.instance = instance;
    }
}
