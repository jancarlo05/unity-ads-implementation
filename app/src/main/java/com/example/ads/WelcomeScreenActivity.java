package com.example.ads;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.ironsource.mediationsdk.ISBannerSize;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.IronSourceBannerLayout;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.BannerListener;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;
import com.unity3d.services.UnityServices;
import com.unity3d.services.banners.BannerErrorInfo;
import com.unity3d.services.banners.BannerView;
import com.unity3d.services.banners.UnityBannerSize;

import java.util.ArrayList;

public class WelcomeScreenActivity extends AppCompatActivity {

    private ConstraintLayout container1;
    private TextView message1,message2,message3;
    private AdView mAdView;
    private CheckBox checkBox;
    private InterstitialAd mInterstitialAd;
    private String unityGameId = "";
    private Boolean testMode = true;
    private String placementId = "090217";
    private String banner2 = "062120204";
    private String banner3 = "062120203";

    private Button season1;
    private Button season2;
    private Button season3,getstarted;
    int position = 0;
    int videoAdsposition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        container1 = findViewById(R.id.container1);
        getstarted = findViewById(R.id.getstarted);

        UnityAds.initialize (this, "3341322");

        Initialize();
        FindBanner();

        getstarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeScreenActivity.this,HomePageActivity.class);
                startActivity(intent);
            }
        });
    }

    private void Initialize(){
        if (UnityAds.isInitialized()){
            System.out.println("-------------- Unity Ads INITIALIZED Ready----------------");
            if (UnityAds.isReady()){
                System.out.println("-------------- Unity Ads Ready----------------");
                LoadBanner(container1,this);
//                LoadPlayableAds(this);
            }else {
                System.out.println("-------------- Unity Ads  NOT Ready----------------");
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                    @Override
                    public void onFinish() {
                        Initialize();
                    }
                }.start();
            }
        }else {
            System.out.println("-------------- Unity Ads INITIALIZED  NOT Ready----------------");
            new CountDownTimer(1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }
                @Override
                public void onFinish() {
                    Initialize();
                }
            }.start();
        }

    }

    private void FindBanner(){


        final Activity myActivity = this;
        // Declare a new banner listener, and set it as the active banner listener:
        // Initialize the Ads SDK:

//        IronSource.init(this, unityGameId, IronSource.AD_UNIT.BANNER);


//        for (String ads : videoAds){
//
//        }


    }

    private void LoadPlayableAds(final Activity activity){
        final boolean[] isPlaying = {false};
        final ArrayList<String>videoAds= new ArrayList<>();
        videoAds.add("0621202014");
        videoAds.add("rewardedVideo");
        videoAds.add("0902173");
        videoAds.add("0902175");
        videoAds.add("062120202");
        videoAds.add("062120208");
        videoAds.add("video");

        IUnityAdsListener listener = new IUnityAdsListener() {
            @Override
            public void onUnityAdsReady(String s) {

            }
            @Override
            public void onUnityAdsStart(String s) {

            }

            @Override
            public void onUnityAdsFinish(String s, UnityAds.FinishState finishState) {
                new CountDownTimer(3000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        Toast.makeText(activity, "START NEW ADS IN : "+millisUntilFinished/1000, Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onFinish() {
                        videoAdsposition++;
                        if (videoAdsposition < videoAds.size()){
                            LoadPlayableAds(activity);
                        }else {
                            videoAdsposition = 0;
                            LoadPlayableAds(activity);
                        }
                    }
                }.start();
            }
            @Override
            public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String s) {
                System.out.println("ERROR : "+s);
                Toast.makeText(activity, ""+s, Toast.LENGTH_SHORT).show();
                videoAdsposition++;
                if (videoAdsposition < videoAds.size()){
                    LoadPlayableAds(activity);
                }else {
                    videoAdsposition = 0;
                    LoadPlayableAds(activity);
                }

            }
        };

        UnityAds.addListener(listener);
        if (UnityAds.isReady(videoAds.get(videoAdsposition))){
            UnityAds.show(activity,videoAds.get(videoAdsposition));
        }else {
            new CountDownTimer(1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }
                @Override
                public void onFinish() {
                    videoAdsposition++;
                    if (videoAdsposition < videoAds.size()){
                        LoadPlayableAds(activity);
                    }else {
                        videoAdsposition = 0;
                        LoadPlayableAds(activity);
                    }
                }
            }.start();
        }


    }


    private void LoadBanner(final ViewGroup container,final Activity activity){
        System.out.println("Banner Started!");

        final ArrayList<String>banners = new ArrayList<>();
        banners.add("062120204");
        banners.add("090217");
        banners.add("0902172");
        banners.add("0902174");
        banners.add("06212020");
        banners.add("062120203");
        banners.add("062120205");
        banners.add("062120206");
        banners.add("062120207");
        banners.add("062120208");
        banners.add("062120209");
        banners.add("0621202010");
        banners.add("0621202011");
        banners.add("0621202012");
        banners.add("0621202013");
        banners.add("0621202015");
        banners.add("0621202016");
        banners.add("0621202017");
        banners.add("0621202018");

        BannerView bannerView = new BannerView(activity,banners.get(position), new UnityBannerSize(320,50));
        bannerView.setListener(new BannerView.IListener() {
            @Override
            public void onBannerLoaded(BannerView bannerView) {
                System.out.println("BANNER FOUND -----------------------------");
                if (container.getChildCount()>2){
                    container.removeAllViews();
                }
                container.addView(bannerView);

                new CountDownTimer(1000 * 60, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                    @Override
                    public void onFinish() {
                        position = 0;
                        LoadBanner(container,activity);
                    }
                }.start();
            }
            @Override
            public void onBannerClick(BannerView bannerView) {

            }
            @Override
            public void onBannerFailedToLoad(BannerView bannerView, BannerErrorInfo bannerErrorInfo) {
                position++;
                System.out.println("Banner Load Failed : " +position);
                if (position < banners.size()){
                    LoadBanner(container,activity);
                }else {
                    position =0;
                    LoadBanner(container,activity);
                }
            }

            @Override
            public void onBannerLeftApplication(BannerView bannerView) {
            }
        });
        bannerView.load();
    }



}
