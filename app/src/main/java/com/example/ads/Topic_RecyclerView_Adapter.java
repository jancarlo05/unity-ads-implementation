package com.example.ads;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class Topic_RecyclerView_Adapter extends RecyclerView.Adapter<Topic_RecyclerView_Adapter.ViewHolder>{

    private ArrayList<Topic>topics;
    private Context context;
    // RecyclerView recyclerView;
    public Topic_RecyclerView_Adapter(ArrayList<Topic>topics, Context context) {
        this.topics = topics;
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.topic_cardview, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Topic topic = topics.get(position);
        holder.title.setText(topic.title);
        Glide.with(context).load(topic.image).into(holder.image);

        Subtitle_listView_adapter adapter = new Subtitle_listView_adapter(context,topic);
        holder.listView.setAdapter(adapter);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataManager.getInstance().setSelectedTopic(topic);
                Intent intent = new Intent(context, SelectedModuleActivity.class);
                context.startActivity(intent);
            }
        });

    }
    @Override
    public int getItemCount() {
        return topics.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView image;
        ListView listView;
        public ViewHolder(View itemView) {
            super(itemView);
            listView = itemView.findViewById(R.id.listView);
            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);

        }
    }
}