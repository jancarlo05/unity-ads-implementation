package com.example.ads;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unity3d.ads.UnityAds;
import com.unity3d.services.banners.BannerErrorInfo;
import com.unity3d.services.banners.BannerView;
import com.unity3d.services.banners.UnityBannerSize;

import java.util.ArrayList;

public class SelectedModuleActivity extends AppCompatActivity {

    private LinearLayout container1;
    ListView listView;
    TextView title;
    ImageView icon;
    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_module);

        listView = findViewById(R.id.listView);
        title = findViewById(R.id.title);
        icon = findViewById(R.id.icon);
        container1 = findViewById(R.id.container1);

        DisplayData();

        LoadBanner(container1,this);
    }

    private void DisplayData(){
        try {
            DataManager dataManager = DataManager.getInstance();
            Topic topic = dataManager.getSelectedTopic();
            Glide.with(this).load(topic.image).into(icon);
            title.setText(topic.title);
            SelectedModule_listView_adapter adapter = new SelectedModule_listView_adapter(this,topic.child,this);
            listView.setAdapter(adapter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void LoadBanner(final ViewGroup container,final Activity activity){
        System.out.println("Banner Started!");

        final ArrayList<String>banners = new ArrayList<>();
        banners.add("062120204");
        banners.add("090217");
        banners.add("0902172");
        banners.add("0902174");
        banners.add("06212020");
        banners.add("062120203");
        banners.add("062120205");
        banners.add("062120206");
        banners.add("062120207");
        banners.add("062120208");
        banners.add("062120209");
        banners.add("0621202010");
        banners.add("0621202011");
        banners.add("0621202012");
        banners.add("0621202013");
        banners.add("0621202015");
        banners.add("0621202016");
        banners.add("0621202017");
        banners.add("0621202018");

        BannerView bannerView = new BannerView(activity,banners.get(position), new UnityBannerSize(320,50));
        bannerView.setListener(new BannerView.IListener() {
            @Override
            public void onBannerLoaded(BannerView bannerView) {
                System.out.println("BANNER FOUND -----------------------------");
                if (container.getChildCount()>2){
                    container.removeAllViews();
                }
                container.addView(bannerView);

                new CountDownTimer(1000 * 60, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                    @Override
                    public void onFinish() {
                        position = 0;
                        LoadBanner(container,activity);
                    }
                }.start();
            }
            @Override
            public void onBannerClick(BannerView bannerView) {

            }
            @Override
            public void onBannerFailedToLoad(BannerView bannerView, BannerErrorInfo bannerErrorInfo) {
                position++;
                System.out.println("Banner Load Failed : " +position);
                if (position < banners.size()){
                    LoadBanner(container,activity);
                }else {
                    position =0;
                    LoadBanner(container,activity);
                }
            }

            @Override
            public void onBannerLeftApplication(BannerView bannerView) {
            }
        });
        bannerView.load();
    }

}
