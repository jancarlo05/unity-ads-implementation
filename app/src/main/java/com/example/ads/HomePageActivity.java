package com.example.ads;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class HomePageActivity extends AppCompatActivity {

    RecyclerView recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        recycler_view = findViewById(R.id.recycler_view);


        ArrayList<Topic>topics = new ArrayList<>();

        ArrayList<String>subtitles = new ArrayList<>();

        subtitles.add("Android Introduction");
        subtitles.add("What is Android");
        subtitles.add("History and Version");
        subtitles.add("Android Architecture");
        subtitles.add("Core Building Blocks");
        subtitles.add("Android Emulator");
        subtitles.add("Install Android");
        subtitles.add("Hello Android example");
        subtitles.add("Internal Details");
        subtitles.add("Dalvik VM");
        subtitles.add("AndroidManifest.xml");
        subtitles.add("R.java");
        subtitles.add("Hide Title Bar");
        subtitles.add("Screen Orientation");
        topics.add(new Topic("Android Studio Tutorial",R.drawable.android_studio_icon,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("UI Widgets");
        subtitles.add("Working with Button");
        subtitles.add("Toast");
        subtitles.add("Custom Toast");
        subtitles.add("ToggleButton");
        subtitles.add("CheckBox");
        subtitles.add("Custom CheckBox");
        subtitles.add("RadioButton");
        subtitles.add("Dynamic RadioButton");
        subtitles.add("Custom RadioButton");
        subtitles.add("AlertDialog");
        subtitles.add("Spinner");
        subtitles.add("AutoCompleteTextView");
        subtitles.add("ListView");
        subtitles.add("Custom ListView");
        subtitles.add("WebView");
        subtitles.add("SeekBar");
        subtitles.add("DatePicker");
        subtitles.add("TimePicker");
        subtitles.add("Analog and Digital");
        subtitles.add("Vertical ScrollView");
        subtitles.add("Horizontal ScrollView");
        subtitles.add("TimePicker");
        subtitles.add("ImageSwitcher");
        subtitles.add("ImageSlider");
        subtitles.add("ViewStub");
        subtitles.add("TabLayout");
        subtitles.add("TabLayout with FrameLayout");
        subtitles.add("SearchView");
        subtitles.add("SearchView on Toolbar");
        subtitles.add("EditText with TextWatcher");
        topics.add(new Topic("Android Widgets",R.drawable.android_widgets,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Activity LifeCycle");
        subtitles.add("Implicit Intent");
        subtitles.add("Explicit Intent");
        subtitles.add("StartActivityForResult");
        subtitles.add("Share App Data");
        topics.add(new Topic("Activity and Intents",R.drawable.android_activity_icon,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Android Fragments");
        topics.add(new Topic("Android Fragments",R.drawable.android_fragment_icon,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Option Menu");
        subtitles.add("Context Menu");
        subtitles.add("Popup Menu");
        topics.add(new Topic("Android Menu",R.drawable.android_menu_icon,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Android Service");
        topics.add(new Topic("Android Service",R.drawable.android_service_icon,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Android AlarmManager");
        topics.add(new Topic("Android AlarmManager",R.drawable.android_alarm,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Android Preferences");
        subtitles.add("nternal Storage");
        subtitles.add("External Storage");
        topics.add(new Topic("Android Storage",R.drawable.android_storage,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("XML Parsing SAX");
        subtitles.add("XML Parsing DOM");
        subtitles.add("XMLPullParser");
        subtitles.add("JSON Parsing");
        topics.add(new Topic("XML and JSON",R.drawable.android_json,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("MediaPlayer: Audio");
        subtitles.add("VideoView: Video");
        subtitles.add("Recording Media");
        topics.add(new Topic("Android Multimedia",R.drawable.android_multimedia,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("TextToSpeech1");
        subtitles.add("TextToSpeech2");
        topics.add(new Topic("Android Speech",R.drawable.android_speech,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("TelephonyManager");
        subtitles.add("Get Call State");
        subtitles.add("Get Call State 2");
        subtitles.add("Simple Caller Talker");
        subtitles.add("Phone Call");
        subtitles.add("Send SMS");
        subtitles.add("Send Email");
        topics.add(new Topic("Android Telephony",R.drawable.telephony,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Bluetooth Tutorial");
        subtitles.add("List Paired Devices");
        subtitles.add("WIFI");
        topics.add(new Topic("Android Device",R.drawable.android_device,subtitles));


        subtitles = new ArrayList<>();
        subtitles.add("Camera Tutorial");
        topics.add(new Topic("Camera Tutorial",R.drawable.android_camera,subtitles));


        subtitles = new ArrayList<>();
        subtitles.add("Sensor Tutorial");
        topics.add(new Topic("Sensor Tutorial",R.drawable.android_sensor,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Android Graphicsl");
        topics.add(new Topic("Android Graphics",R.drawable.android_graphics,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Android Animation");
        topics.add(new Topic("Android Animation",R.drawable.android_animation,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Android Web Service");
        topics.add(new Topic("Android Web Service",R.drawable.android_web_service,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Android Google Map");
        subtitles.add("Current Location");
        subtitles.add("Search Location");
        topics.add(new Topic("Google Map",R.drawable.android_map,subtitles));

        subtitles = new ArrayList<>();
        subtitles.add("Android Google Admob");
        subtitles.add("1) Banner Ads");
        subtitles.add("2) Interstitial Ads");
        topics.add(new Topic("Adding Ads",R.drawable.android_ads,subtitles));


        subtitles = new ArrayList<>();
        subtitles.add("QR/Bar Code Scanner");
        subtitles.add("RSS Feed Reader");
        subtitles.add("Volley Fetch JSON");
        subtitles.add("Android Linkify");
        subtitles.add("Intro Slider");
        subtitles.add("RecyclerView List");
        subtitles.add("Swipe Del RecyclerView");
        subtitles.add("Swipe Refresh Activity");
        subtitles.add("Volley Library Registration Log-in Log-out");
        subtitles.add("Network Connectivity Services");
        subtitles.add("Firebase Authentication - Google Login");
        subtitles.add("Android Notification");
        subtitles.add("Using Google reCAPTCHA");
        topics.add(new Topic("Android Examples",R.drawable.android_example,subtitles));


        Topic_RecyclerView_Adapter adapter = new Topic_RecyclerView_Adapter(topics,this);
        recycler_view.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        recycler_view.setAdapter(adapter);


    }
}
