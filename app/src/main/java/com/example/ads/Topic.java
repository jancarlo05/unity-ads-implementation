package com.example.ads;

import java.util.ArrayList;

public class Topic {
    public String title;
    public int image;
    public ArrayList<String>child;

    public Topic(String title, int image, ArrayList<String> child) {
        this.title = title;
        this.image = image;
        this.child = child;
    }

    public Topic(String title, int image) {
        this.title = title;
        this.image = image;
    }

    public Topic(String title) {
        this.title = title;
    }
}
